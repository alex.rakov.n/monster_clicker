﻿using UnityEngine;

namespace Game
{
    public class GameStateContainer : MonoBehaviour
    {
        private static GameState _currentGameState;

        public static GameState CurrentGameState => _currentGameState;

        private void Start()
        {
            _currentGameState = GameState.Stopped;
        }

        public void ChangeGameState(GameState state)
        {
            _currentGameState = state;
        }
    }
}