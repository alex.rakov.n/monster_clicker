﻿using System;
using Abstraction;
using Enemies;
using UI;
using UnityEngine;

namespace Game
{
    public class ScoreContainer : MonoBehaviour, ISubscription<Action<int>>
    {
        private event Action<int> ScoreChanged;
        private int _currentScore = 0;
        private EnemiesPool _enemiesPool;
        private RestartGameButton _restartButton;

        public int CurrentScore => _currentScore;

        private void Awake()
        {
            _enemiesPool = FindObjectOfType<EnemiesPool>();
            _enemiesPool.SubscribeToDieEvent(IncreaseScore);

            _restartButton = FindObjectOfType<RestartGameButton>();
            _restartButton.Subscribe(ResetScore);
        }
        
        private void IncreaseScore(Enemy _)
        {
            _currentScore++;
            ScoreChanged?.Invoke(_currentScore);
        }

        private void ResetScore()
        {
            _currentScore = 0;
            ScoreChanged?.Invoke(_currentScore);
        }

        public void Subscribe(Action<int> method)
        {
            ScoreChanged += method;
        }

        public void Unsubscribe(Action<int> method)
        {
            ScoreChanged -= method;
        }

        private void OnDestroy()
        {
            _restartButton.Unsubscribe(ResetScore);
        }
    }
}