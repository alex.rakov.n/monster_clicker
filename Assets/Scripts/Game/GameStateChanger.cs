using System;
using Abstraction;
using Enemies;
using UI;
using UnityEngine;

namespace Game
{
    public class GameStateChanger : MonoBehaviour, ISubscription<Action<GameState>>
    {
        private event Action<GameState> StateChanged;
        private FailChecker _failChecker;
        private GameStateContainer _stateContainer;
        private StartGameButton _startButton;
        private RestartGameButton _restartButton;

        private void Awake()
        {
        
            _stateContainer = FindObjectOfType<GameStateContainer>();

            _failChecker = FindObjectOfType<FailChecker>();
            _failChecker.Subscribe(StopGame);

            _startButton = FindObjectOfType<StartGameButton>();
            _startButton.Subscribe(StartGame);

            _restartButton = FindObjectOfType<RestartGameButton>();
            _restartButton.Subscribe(StartGame);
        }

        private void StopGame()
        {
            _stateContainer.ChangeGameState(GameState.Stopped);
            StateChanged?.Invoke(GameState.Stopped);
        }

        private void StartGame()
        {
            _stateContainer.ChangeGameState(GameState.Active);
            StateChanged?.Invoke(GameState.Active);
        }

        private void OnDestroy()
        {
            _failChecker.Unsubscribe(StopGame);
            _startButton.Unsubscribe(StartGame);
            _restartButton.Unsubscribe(StartGame);
        }

        public void Subscribe(Action<GameState> method)
        {
            StateChanged += method;
        }

        public void Unsubscribe(Action<GameState> method)
        {
            StateChanged -= method;
        }
    }
}
