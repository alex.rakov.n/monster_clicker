using System;
using Abstraction;
using Enemies;
using UnityEngine;

namespace Game
{
    public class FailChecker : MonoBehaviour, ISubscription<Action>
    {
        [SerializeField] private int _allowedEnemiesCount = 10;
        private event Action FailHappened;
        private EnemiesPool _enemiesPool;

        private void Start()
        {
            _enemiesPool = FindObjectOfType<EnemiesPool>();
        }

        private void Update()
        {
            if (_enemiesPool.ActiveEnemiesCount == _allowedEnemiesCount)
            {
                FailHappened?.Invoke();
            }
        }

        public void Subscribe(Action method)
        {
            FailHappened += method;
        }

        public void Unsubscribe(Action method)
        {
            FailHappened -= method;
        }
    }
}
