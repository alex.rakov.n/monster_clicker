﻿using System;
using Abstraction;
using UnityEngine;

namespace Game
{
    public class ComplexityIncreaser : MonoBehaviour, ISubscription<Action>
    {
        [SerializeField] private int _enemyAmountToIncrease = 10;
        private event Action ComplexityIncreased;
        private ScoreContainer _scoreContainer;
        private LevelContainer _levelContainer;
        private int _lastIncrease = 0;

        private void Start()
        {
            _scoreContainer = FindObjectOfType<ScoreContainer>();
            _scoreContainer.Subscribe(IncreaseComplexity);
            _levelContainer = FindObjectOfType<LevelContainer>();
        }

        private void IncreaseComplexity(int value)
        {
            int newIncrease = _scoreContainer.CurrentScore / _enemyAmountToIncrease;
            if(newIncrease > _lastIncrease)
            {
                _lastIncrease = newIncrease;
                _levelContainer.IncreaseLevel();
                ComplexityIncreased?.Invoke();
            }
        }

        public void Subscribe(Action method)
        {
            ComplexityIncreased += method;
        }

        public void Unsubscribe(Action method)
        {
            ComplexityIncreased -= method;
        }
    }
}