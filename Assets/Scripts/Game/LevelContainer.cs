using UnityEngine;

namespace Game
{
    public class LevelContainer : MonoBehaviour
    {
        [SerializeField] private LevelConfig[] _levels;

        private LevelConfig _currentLevel;
        private int _levelIndex = 0;

        public LevelConfig CurrentLevel => _currentLevel;

        private void Awake()
        {
            _currentLevel = _levels[_levelIndex];
        }

        public void IncreaseLevel()
        {
            _levelIndex++;
            if (_levelIndex < _levels.Length)
            {
                _currentLevel = _levels[_levelIndex];
            }
        }

        public void ResetLevel()
        {
            _levelIndex = 0;
            _currentLevel = _levels[_levelIndex];
        }
    }
}
