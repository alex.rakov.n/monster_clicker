﻿using Abstraction;
using UnityEngine;

namespace Enemies
{
    [RequireComponent(typeof(ParticleSystemsPool))]
    public class SpawnParticleSystemPlacer : ParticleSystemPlacer<Enemy>
    {
        private Spawner _spawner;
        protected override void Start()
        {
            base.Start();
            _spawner = FindObjectOfType<Spawner>();
            _spawner.Subscribe(PlaceParticleSystem);
        }

        private void OnDestroy()
        {
            _spawner.Unsubscribe(PlaceParticleSystem);
        }
    }
}