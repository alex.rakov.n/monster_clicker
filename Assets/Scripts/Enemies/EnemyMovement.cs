using Game;
using UnityEngine;
using UnityEngine.AI;

namespace Enemies
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemyMovement : MonoBehaviour
    {
        private NavMeshAgent _agent;
        private GroundBounds _groundBounds;
        private const float RemainingDistanceOffset = 0.01f;
        private bool _canMove = true;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _groundBounds = FindObjectOfType<GroundBounds>();
        }

        private void OnEnable()
        {
            _canMove = true;
            _agent.SetDestination(_groundBounds.GetRandomPosition(0));
        }

        private void OnDisable()
        {
            _canMove = false;
        }

        private void Update()
        {
            if (_canMove && GameStateContainer.CurrentGameState == GameState.Active && _agent.remainingDistance <= RemainingDistanceOffset)
            {
                _agent.SetDestination(_groundBounds.GetRandomPosition(0));
            }
        }

        public void StopMoving()
        {
            _canMove = false;
        }

        public void SetSpeed(float speed)
        {
            _agent.speed = speed;
        }
    }
}
