﻿using UnityEngine;

namespace Enemies
{
    public class DeathHandler : MonoBehaviour
    {
        private EnemiesPool _pool;

        private void Awake()
        {
            _pool = FindObjectOfType<EnemiesPool>();
            _pool.SubscribeToDieEvent(HandleDeath);
        }

        private void HandleDeath(Enemy enemy)
        {
            _pool.ReturnObject(enemy);
        }
    }
}