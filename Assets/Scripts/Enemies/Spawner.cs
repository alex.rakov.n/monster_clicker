using System;
using System.Collections;
using Abstraction;
using Game;
using UnityEngine;

namespace Enemies
{
    [RequireComponent(typeof(EnemiesPool))]
    public class Spawner : MonoBehaviour, IStateChangesHandler, ISubscription<Action<Enemy>>
    {
        [SerializeField] private float _heightSpawn = 4;
        private GroundBounds _groundBounds;
        private EnemiesPool _enemiesPool;
        private LevelContainer _levelContainer;
        private Coroutine _spawnCoroutine;
        private GameStateChanger _stateChanger;
        private event Action<Enemy> EnemySpawned;
    
        private void Start()
        {
            _enemiesPool = GetComponent<EnemiesPool>();

            _groundBounds = FindObjectOfType<GroundBounds>();
            _levelContainer = FindObjectOfType<LevelContainer>();
            _stateChanger = FindObjectOfType<GameStateChanger>();
            _stateChanger.Subscribe(HandleState);
        }

        private IEnumerator SpawnCoroutine()
        {
            while (true)
            {
                Spawn();
                yield return new WaitForSeconds(_levelContainer.CurrentLevel.SpawnRate);
            }
        }

        private void Spawn()
        {
            Vector3 spawnPosition = _groundBounds.GetRandomPosition(_heightSpawn);

            Enemy spawnedEnemy = _enemiesPool.GetObject();
            spawnedEnemy.transform.position = spawnPosition;
            spawnedEnemy.UpdateParameters(_levelContainer.CurrentLevel.EnemySpeed, _levelContainer.CurrentLevel.EnemyHp);
            spawnedEnemy.Activate();
            EnemySpawned?.Invoke(spawnedEnemy);
        }

        public void StopSpawn()
        {
            StopCoroutine(_spawnCoroutine);
        }

        public void StartSpawn()
        {
            _spawnCoroutine = StartCoroutine(SpawnCoroutine());
        }

        public void HandleState(GameState state)
        {
            if (state == GameState.Active)
            {
                StartSpawn();
            }
            else
            {
                StopSpawn();
            }
        }

        public void Subscribe(Action<Enemy> method)
        {
            EnemySpawned += method;
        }

        public void Unsubscribe(Action<Enemy> method)
        {
            EnemySpawned -= method;
        }
    }
}
