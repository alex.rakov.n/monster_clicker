﻿using Abstraction;
using UnityEngine;

namespace Enemies
{
    [RequireComponent(typeof(ParticleSystemsPool))]
    public class DeathParticleSystemPlacer : ParticleSystemPlacer<Enemy>
    {
        private EnemiesPool _enemiesPool;

        protected override void Start()
        {
            base.Start();
            _enemiesPool = FindObjectOfType<EnemiesPool>();
            _enemiesPool.SubscribeToDieEvent(PlaceParticleSystem);
        }
    }
}