using Abstraction;
using Game;
using UnityEngine;

namespace Enemies
{
    [RequireComponent(typeof(EnemyHealth)), RequireComponent(typeof(EnemyMovement))]
    public class Enemy : MonoBehaviour, IPoolable
    {
        private EnemyHealth _health;
        private EnemyMovement _movement;

        public Health<Enemy> Health => _health;

        private void Awake()
        {
            _health = GetComponent<EnemyHealth>();
            _movement = GetComponent<EnemyMovement>();
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        public void Activate()
        {
            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            if (GameStateContainer.CurrentGameState == GameState.Active && !_health.IsDeath)
            {
                _health.Kill();
            }
            gameObject.SetActive(false);
        }

        public void UpdateParameters(float speed, int health)
        {
            _movement.SetSpeed(speed);
            _health.SetHealth(health);
        }
    }
}
