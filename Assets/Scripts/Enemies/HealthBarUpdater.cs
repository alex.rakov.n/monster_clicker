using UnityEngine;
using UnityEngine.UI;

namespace Enemies
{
    [RequireComponent(typeof(Slider))]
    public class HealthBarUpdater : MonoBehaviour
    {
        private Slider _healthBar;
        private EnemyHealth _enemyHealth;
        private Camera _camera;

        private void Start()
        {
            _camera = Camera.main;
            _healthBar = GetComponent<Slider>();
            _enemyHealth = GetComponentInParent<EnemyHealth>();
        }

        private void Update()
        {
            _healthBar.value = _enemyHealth.CurrentHealth / (float)_enemyHealth.MaxHealth;
            transform.LookAt(_camera.transform);
        }
    }
}
