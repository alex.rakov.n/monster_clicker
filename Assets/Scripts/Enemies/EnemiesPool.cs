using System;
using System.Collections.Generic;
using Abstraction;
using UnityEngine;

namespace Enemies
{
    public class EnemiesPool : ObjectPool<Enemy>
    {
        [SerializeField] private Enemy _enemyPrefab;
        private List<Action<Enemy>> _subscribers;

        public int ActiveEnemiesCount => _activeObjects.Count;

        protected override void Awake()
        {
            base.Awake();
            _subscribers ??= new List<Action<Enemy>>();
            
        }

        protected override void InitializePool(int number)
        {
            for (int i = 0; i < number; i++)
            {
                _pool.Add(CreateNewObject());
            }
        }

        protected override Enemy CreateNewObject()
        {
            Enemy newEnemy = Instantiate(_enemyPrefab, transform);
            newEnemy.Deactivate();
            Subscription(newEnemy);
            return newEnemy;
        }

        private void Subscription(Enemy enemy)
        {
            foreach (var subscriber in _subscribers)
            {
                enemy.Health.Subscribe(subscriber);;
            }
        }

        public virtual void SubscribeToDieEvent(Action<Enemy> action)
        {
            _subscribers ??= new List<Action<Enemy>>();
            _subscribers.Add(action);
        }

        protected virtual void UnsubscribeAll()
        {
            foreach (var enemy in _pool)
            {
                foreach (var subscriber in _subscribers)
                {
                    enemy.Health.Unsubscribe(subscriber);
                }
            }
        }

        private void OnDestroy()
        {
            UnsubscribeAll();
        }
    }
}
