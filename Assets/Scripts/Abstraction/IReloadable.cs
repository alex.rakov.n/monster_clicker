﻿using System.Collections;

namespace Abstraction
{
    public interface IReloadable
    {
        public IEnumerator Reload();
    }
}