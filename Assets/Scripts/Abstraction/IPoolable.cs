﻿namespace Abstraction
{
    public interface IPoolable
    {
        public bool IsActive();
        public void Activate();
        public void Deactivate();
    }
}