﻿namespace Abstraction
{
    public interface IScreenEnabler
    {
        public void ActivateScreen();
        public void DeactivateScreen();
    }
}