namespace Abstraction
{
    public interface IBooster
    {
        public void ApplyBooster();
    }
}
