﻿using System;
using System.Collections;
using UnityEngine;

namespace Abstraction
{
    [RequireComponent(typeof(GameButton))]
    public abstract class BaseBooster : MonoBehaviour, IBooster, ISubscription<Action<bool>>, IReloadable
    {
        [SerializeField] protected float _reloadTime;
        protected event Action<bool> ChangedUsability; 
        protected bool _canUse = true;
        protected GameButton _button;

        public bool CanUse => _canUse;

        protected virtual void Awake()
        {
            _button = GetComponent<GameButton>();
            _button.Subscribe(ApplyBooster);
        }

        protected virtual void OnEnable()
        {
            StopAllCoroutines();
            _canUse = true;
            Invoke();
        }

        public abstract void ApplyBooster();
        
        public void Subscribe(Action<bool> method)
        {
            ChangedUsability += method;
        }

        public void Unsubscribe(Action<bool> method)
        {
            ChangedUsability -= method;
        }

        protected void Invoke()
        {
            ChangedUsability?.Invoke(_canUse);
        }

        public virtual IEnumerator Reload()
        {
            yield return new WaitForSeconds(_reloadTime);
            _canUse = true;
            Invoke();
            yield return null;
        }

        protected void OnDestroy()
        {
            _button.Unsubscribe(ApplyBooster);
        }
    }
}