using System;
using UnityEngine;

namespace Abstraction
{
    public class Health<T> : MonoBehaviour, ISubscription<Action<T>> where T : MonoBehaviour
    {
        private event Action<T> DieEvent;

        private T _baseType;
        private int _currentHealth;
        private int _maxHealth;

        public int CurrentHealth => _currentHealth;
        public int MaxHealth => _maxHealth;
        public bool IsDeath => _currentHealth <= 0;

        private void Awake()
        {
            _baseType = GetComponent<T>();
        }

        public void DecreaseHealth(int amount = 1)
        {
            _currentHealth -= amount;
            if (_currentHealth <= 0)
            {
                DieEvent?.Invoke(_baseType);
            }
        }

        public void SetHealth(int amount)
        {
            _maxHealth = amount;
            _currentHealth = amount;
        }

        public void Subscribe(Action<T> method)
        {
            DieEvent += method;
        }

        public void Unsubscribe(Action<T> method)
        {
            DieEvent -= method;
        }

        public void Kill()
        {
            _currentHealth = 0;
            DecreaseHealth();
        }
    }
}
