﻿using Game;

namespace Abstraction
{
    public interface IStateChangesHandler
    {
        public void HandleState(GameState state);
    }
}