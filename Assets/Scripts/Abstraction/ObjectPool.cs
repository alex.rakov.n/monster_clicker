using System.Collections.Generic;
using UnityEngine;

namespace Abstraction
{
    public abstract class ObjectPool<T> : MonoBehaviour where T : IPoolable
    {
        [SerializeField] protected int _startBuffer;
        protected List<T> _pool;
        protected List<T> _activeObjects;

        public List<T> ActiveObjects => _activeObjects;
        protected virtual void Awake()
        {
            _pool = new List<T>();
            _activeObjects = new List<T>();
        }

        protected virtual void Start()
        {
            InitializePool(_startBuffer);
        }

        protected abstract void InitializePool(int number);
        protected abstract T CreateNewObject();

        public virtual T GetObject()
        {
            foreach (var obj in _pool)
            {
                if (!obj.IsActive())
                {
                    _activeObjects.Add(obj);
                    return obj;
                }
            }

            T newObj = CreateNewObject();
            _pool.Add(newObj);
            _activeObjects.Add(newObj);
        
            return newObj;
        }

        public virtual void ReturnObject(T obj)
        {
            _activeObjects.Remove(obj);
            obj.Deactivate();
        }

        public virtual void ReturnAllActiveObjects()
        {
            foreach (var obj in _pool)
            {
                if(obj.IsActive())
                    ReturnObject(obj);
            }
        }
    
    }
}