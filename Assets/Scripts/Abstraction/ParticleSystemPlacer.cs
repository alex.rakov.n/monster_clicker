﻿using UnityEngine;

namespace Abstraction
{
    [RequireComponent(typeof(ParticleSystemsPool))]
    public class ParticleSystemPlacer<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected ParticleSystemsPool _pool;

        protected virtual void Start()
        {
            _pool = GetComponent<ParticleSystemsPool>();
        }

        protected virtual void PlaceParticleSystem(T obj)
        {
            PoolableParticleSystem system = _pool.GetObject();
            system.transform.position = obj.transform.position;
            system.Activate();
        }
    }
}