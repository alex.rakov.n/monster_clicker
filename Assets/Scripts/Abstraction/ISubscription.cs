﻿using System;

namespace Abstraction
{
    public interface ISubscription<in T> where T : Delegate
    {
        public void Subscribe(T method);
        public void Unsubscribe(T method);
    }
}