﻿namespace Abstraction
{
    public interface IStopableBooster : IBooster
    {
        public void StopBooster();
    }
}