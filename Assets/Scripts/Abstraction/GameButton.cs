﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Abstraction
{
    [RequireComponent(typeof(Button))]
    public class GameButton : MonoBehaviour, ISubscription<Action>
    {
        private event Action ButtonClicked;
        protected Button _button;

        protected virtual void Start()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnClick);
        }

        protected virtual void OnClick()
        {
            ButtonClicked?.Invoke();
        }

        public virtual void Subscribe(Action action)
        {
            ButtonClicked += action;
        }

        public virtual void Unsubscribe(Action action)
        {
            ButtonClicked -= action;
        }

        protected void OnDestroy()
        {
            if (_button != null) 
                _button.onClick?.RemoveListener(OnClick);
        }
    }
}