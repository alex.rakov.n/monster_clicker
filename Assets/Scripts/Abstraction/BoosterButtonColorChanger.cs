﻿using UnityEngine;
using UnityEngine.UI;

namespace Abstraction
{
    [RequireComponent(typeof(Button)), RequireComponent(typeof(BaseBooster))]
    public class BoosterButtonColorChanger : MonoBehaviour
    {
        protected Color _canUseColor = Color.green;
        protected Color _canNotUseColor = Color.red;
        protected Button _button;
        protected BaseBooster _booster;

        protected virtual void Awake()
        {
            _button = GetComponent<Button>();
            ColorBlock buttonColorBlock = _button.colors;
            buttonColorBlock.normalColor = _canUseColor;
            buttonColorBlock.selectedColor = _canUseColor;
            buttonColorBlock.disabledColor = _canNotUseColor;
            _button.colors = buttonColorBlock;

            _booster = GetComponent<BaseBooster>();
            _booster.Subscribe(ChangeButtonColor);
        }

        protected virtual void ChangeButtonColor(bool canUse)
        {
            _button.interactable = canUse;
        }

        protected virtual void OnDestroy()
        {
            _booster.Unsubscribe(ChangeButtonColor);
        }
    }
}