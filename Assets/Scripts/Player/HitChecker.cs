using Enemies;
using UnityEngine;

namespace Player
{
    public class HitChecker : MonoBehaviour
    {
        private InputRaycaster _inputRaycaster;

        private void Awake()
        {
            _inputRaycaster = FindObjectOfType<InputRaycaster>();
            _inputRaycaster.HitHappened += CheckHit;
        }

        private void CheckHit(Collider hitCollider)
        {
            if (hitCollider.TryGetComponent(out Enemy enemy))
            {
                enemy.Health.DecreaseHealth();
            }
        }

        private void OnDestroy()
        {
            _inputRaycaster.HitHappened -= CheckHit;
        }
    }
}
