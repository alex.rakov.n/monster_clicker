using System;
using Game;
using UnityEngine;

namespace Player
{
    public class InputHandler : MonoBehaviour
    {
        public event Action<Vector2> InputHappened;

        private void Update()
        {
            if (GameStateContainer.CurrentGameState == GameState.Active && Input.touchCount > 0)
            {
                Touch currentTouch = Input.GetTouch(0);
                if(currentTouch.phase == TouchPhase.Began)
                    InputHappened?.Invoke(currentTouch.position);
            }
        }
    }
}
