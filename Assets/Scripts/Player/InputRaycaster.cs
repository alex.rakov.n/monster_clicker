using System;
using UnityEngine;

namespace Player
{
    public class InputRaycaster : MonoBehaviour
    {
        public event Action<Collider> HitHappened; 
        private InputHandler _inputHandler;
        private Camera _mainCamera;

        private void Start()
        {
            _inputHandler = FindObjectOfType<InputHandler>();
            _inputHandler.InputHappened += MakeRaycast;
        
            _mainCamera = Camera.main;
        }

        private void MakeRaycast(Vector2 screenPosition)
        {
            Ray ray = _mainCamera.ScreenPointToRay(screenPosition);

            
            if (Physics.Raycast(ray, out RaycastHit info))
            {
                Debug.DrawRay(ray.origin, info.point - ray.origin, Color.red, 0.5f, true);
                HitHappened?.Invoke(info.collider);
            }
        }

        private void OnDestroy()
        {
            _inputHandler.InputHappened -= MakeRaycast;
        }
    }
}
