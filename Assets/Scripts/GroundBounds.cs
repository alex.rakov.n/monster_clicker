using UnityEngine;

public class GroundBounds : MonoBehaviour
{
    private Vector3 _leftDownPosition;
    private Vector3 _rightUpPosition;

    public Vector3 LeftDownPosition => _leftDownPosition;
    public Vector3 RightUpPosition => _rightUpPosition;

    private void Awake()
    {
        Vector3 groundPosition = transform.position;
        Vector3 groundScale = transform.lossyScale;
        
        _leftDownPosition = new Vector3(groundPosition.x - groundScale.x / 2, groundPosition.y, groundPosition.z - groundScale.z / 2);
        _rightUpPosition = new Vector3(groundPosition.x + groundScale.x / 2, groundPosition.y, groundPosition.z + groundScale.z / 2);
    }

    public Vector3 GetRandomPosition(float yComponent)
    {
        float x = Random.Range(_leftDownPosition.x + 1, _rightUpPosition.x - 1);
        float y = _leftDownPosition.y + yComponent;
        float z = Random.Range(_leftDownPosition.z + 1, _rightUpPosition.z - 1);
        return new Vector3(x, y, z);
    }
}
