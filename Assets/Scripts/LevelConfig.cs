using UnityEngine;

[CreateAssetMenu(fileName = "New Level Config", menuName = "Level Configuration")]
public class LevelConfig : ScriptableObject
{
    [SerializeField] private float _spawnRate;
    [SerializeField] private float _enemySpeed;
    [SerializeField] private int _enemyHp;

    public float SpawnRate => _spawnRate;
    public float EnemySpeed => _enemySpeed;
    public int EnemyHp => _enemyHp;
}
