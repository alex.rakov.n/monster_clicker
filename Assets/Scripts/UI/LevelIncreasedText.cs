﻿using System.Collections;
using Game;
using TMPro;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class LevelIncreasedText : MonoBehaviour
    {
        [SerializeField] private float _timeOfShowing = 3f;
        private ComplexityIncreaser _increaser;
        private Coroutine _currentCoroutine;

        private void Start()
        {
            _increaser = FindObjectOfType<ComplexityIncreaser>();
            _increaser.Subscribe(ShowText);
            gameObject.SetActive(false);
        }

        private void ShowText()
        {
            StopAllCoroutines();
            gameObject.SetActive(true);
            _currentCoroutine = StartCoroutine(ShowTextWithTimer());
        }

        private IEnumerator ShowTextWithTimer()
        {
            yield return new WaitForSeconds(_timeOfShowing);
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            _increaser.Unsubscribe(ShowText);
        }
    }
}