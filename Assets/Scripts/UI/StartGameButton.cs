using Abstraction;

namespace UI
{
    public class StartGameButton : GameButton
    {
        private MainMenu _menu;
        protected override void Start()
        {
            base.Start();
            _menu = GetComponentInParent<MainMenu>();
        }

        protected override void OnClick()
        {
            base.OnClick();
            _menu.gameObject.SetActive(false);
        }
    }
}
