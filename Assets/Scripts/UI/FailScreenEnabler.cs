﻿using Abstraction;
using Game;
using UnityEngine;

namespace UI
{
    public class FailScreenEnabler : MonoBehaviour, IScreenEnabler
    {
        private RestartGameButton _restartButton;
        private FailChecker _failChecker;

        private void Start()
        {
            _restartButton = FindObjectOfType<RestartGameButton>();
            _restartButton.Subscribe(DeactivateScreen);

            _failChecker = FindObjectOfType<FailChecker>();
            _failChecker.Subscribe(ActivateScreen);
            
            gameObject.SetActive(false);
        }
        public void ActivateScreen()
        {
            gameObject.SetActive(true);
        }

        public void DeactivateScreen()
        {
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            _restartButton.Unsubscribe(DeactivateScreen);
            _failChecker.Unsubscribe(ActivateScreen);
        }
    }
}