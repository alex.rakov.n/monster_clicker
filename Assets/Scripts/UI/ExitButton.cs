﻿using Abstraction;
using UnityEngine;

namespace UI
{
    public class ExitButton : GameButton
    {
        protected override void OnClick()
        {
            base.OnClick();
            Application.Quit();
        }
    }
}