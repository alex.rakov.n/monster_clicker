using Game;
using TMPro;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class FinalScoreUpdater : MonoBehaviour
    {
        private TMP_Text _text;
        private FailChecker _failChecker;
        private ScoreContainer _scoreContainer;

        private void Awake()
        {
            _text = GetComponent<TMP_Text>();

            _scoreContainer = FindObjectOfType<ScoreContainer>();
            _failChecker = FindObjectOfType<FailChecker>();
            _failChecker.Subscribe(UpdateText);
        }

        private void UpdateText()
        {
            _text.text = "Your score: " + _scoreContainer.CurrentScore;
        }

        private void OnDestroy()
        {
            _failChecker.Unsubscribe(UpdateText);
        }
    }
}
