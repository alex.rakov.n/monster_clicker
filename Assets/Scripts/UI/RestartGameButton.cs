﻿using Abstraction;
using Enemies;
using Game;

namespace UI
{
    public class RestartGameButton : GameButton
    {
        private Spawner _spawner;
        private EnemiesPool _pool;
        private LevelContainer _levelContainer;

        protected override void Start()
        {
            base.Start();
            _spawner = FindObjectOfType<Spawner>();
            _pool = FindObjectOfType<EnemiesPool>();
            _levelContainer = FindObjectOfType<LevelContainer>();
        }

        protected override void OnClick()
        {
            Restart();
            base.OnClick();
        }

        private void Restart()
        {
            _spawner.StopSpawn();
            _pool.ReturnAllActiveObjects();
            _levelContainer.ResetLevel();
        }
    }
}