﻿using Abstraction;
using Game;
using UnityEngine;

namespace UI
{
    public class GameScreenEnabler : MonoBehaviour, IScreenEnabler
    {
        private StartGameButton _startButton;
        private RestartGameButton _restartButton;
        private FailChecker _failChecker;

        private void Awake()
        {
            _startButton = FindObjectOfType<StartGameButton>();
            _startButton.Subscribe(ActivateScreen);

            _restartButton = FindObjectOfType<RestartGameButton>();
            _restartButton.Subscribe(ActivateScreen);

            _failChecker = FindObjectOfType<FailChecker>();
            _failChecker.Subscribe(DeactivateScreen);

            gameObject.SetActive(false);
        }

        public void ActivateScreen()
        {
            gameObject.SetActive(true);
        }

        public void DeactivateScreen()
        {
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            _startButton.Unsubscribe(ActivateScreen);
            _restartButton.Unsubscribe(ActivateScreen);
            _failChecker.Unsubscribe(DeactivateScreen);
        }
    }
}