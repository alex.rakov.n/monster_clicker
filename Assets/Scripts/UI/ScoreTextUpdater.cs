﻿using Game;
using TMPro;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class ScoreTextUpdater : MonoBehaviour
    {
        [SerializeField] private string _textTemplate = "Score: ";
        
        private TMP_Text _text;
        private ScoreContainer _container;
        private void Awake()
        {
            _text = GetComponent<TMP_Text>();
            
            _container = FindObjectOfType<ScoreContainer>();
            _container.Subscribe(UpdateText);
        }

        private void OnEnable()
        {
            UpdateText(_container.CurrentScore);
        }
        
        private void UpdateText(int currentScore)
        {
            _text.text = _textTemplate + currentScore;
        }

        private void OnDestroy()
        {
            _container.Unsubscribe(UpdateText);
        }
    }
}