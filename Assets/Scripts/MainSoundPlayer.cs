using UI;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MainSoundPlayer : MonoBehaviour
{
    private StartGameButton _button;
    private AudioSource _audio;

    private void Start()
    {
        _audio = GetComponent<AudioSource>();
        _button = FindObjectOfType<StartGameButton>();
        _button.Subscribe(_audio.Play);
    }

    private void OnDestroy()
    {
        _button.Unsubscribe(_audio.Play);
    }
}
