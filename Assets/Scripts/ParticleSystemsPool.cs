﻿using Abstraction;
using UnityEngine;

public class ParticleSystemsPool : ObjectPool<PoolableParticleSystem>
{
    [SerializeField] protected PoolableParticleSystem _prefab;

    protected override void InitializePool(int number)
    {
        for (int i = 0; i < number; i++)
        {
            _pool.Add(CreateNewObject());
        }
    }

    protected override PoolableParticleSystem CreateNewObject()
    {
        PoolableParticleSystem newParticleSystem = Instantiate(_prefab, transform);
        newParticleSystem.Subscribe(OnParticleSystemFinished);
        newParticleSystem.Deactivate();
        return newParticleSystem;
    }

    private void OnParticleSystemFinished(PoolableParticleSystem finishedSystem)
    {
        ReturnObject(finishedSystem);
    }
}