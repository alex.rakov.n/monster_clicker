﻿using Abstraction;
using Enemies;
using Game;

namespace Boosters
{
    public class KillAllBooster : BaseBooster
    {
        private EnemiesPool _pool;
        private ScoreContainer _container;

        private void Start()
        {
            _pool = FindObjectOfType<EnemiesPool>();
            _container = FindObjectOfType<ScoreContainer>();
        }

        public override void ApplyBooster()
        {
            if (_canUse)
            {
                _pool.ReturnAllActiveObjects();
                _canUse = false;
                Invoke();
                StartCoroutine(Reload());
            }
        }
    }
}