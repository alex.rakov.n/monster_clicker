using System;
using System.Collections;
using Abstraction;
using Enemies;
using UI;
using UnityEngine;

namespace Boosters
{
    public class FreezeSpawnBooster : BaseBooster, IStopableBooster
    {
        [SerializeField] private float _duration = 3f;

        private Spawner _spawner;
        private Coroutine _currentCoroutine;

        private void Start()
        {
            _spawner = FindObjectOfType<Spawner>();
        }

        public override void ApplyBooster()
        {
            if (_canUse)
            {
                _canUse = false;
                Invoke();
                _currentCoroutine ??= StartCoroutine(FreezeSpawn());
            }
        }

        private IEnumerator FreezeSpawn()
        {
            _spawner.StopSpawn();
            yield return new WaitForSeconds(_duration);
            _spawner.StartSpawn();
            _currentCoroutine = null;
            yield return StartCoroutine(Reload());
        }

        public void StopBooster()
        {
            if (_currentCoroutine != null)
            {
                StopCoroutine(_currentCoroutine);
            }
        }
    }
}