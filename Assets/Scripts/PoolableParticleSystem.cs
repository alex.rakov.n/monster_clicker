﻿using System;
using Abstraction;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class PoolableParticleSystem : MonoBehaviour, IPoolable, ISubscription<Action<PoolableParticleSystem>>
{
    private ParticleSystem _particleSystem;
    private event Action<PoolableParticleSystem> FinishedPlaying;

    private void Awake()
    {
        _particleSystem = GetComponent<ParticleSystem>();
    }

    public bool IsActive()
    {
        return _particleSystem.IsAlive();
    }

    public void Activate()
    {
        ParticleSystem.MainModule currentMain = _particleSystem.main;
        currentMain.stopAction = ParticleSystemStopAction.Callback;
        gameObject.SetActive(true);
        _particleSystem.Play();
    }

    private void OnParticleSystemStopped()
    {
        FinishedPlaying?.Invoke(this);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

    public void Subscribe(Action<PoolableParticleSystem> method)
    {
        FinishedPlaying += method;
    }

    public void Unsubscribe(Action<PoolableParticleSystem> method)
    {
        FinishedPlaying -= method;
    }
}